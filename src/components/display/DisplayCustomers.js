import React from 'react'
import './Display.css'

export class DisplayCustomers extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            users: [],
            isLoaded: false,
            error: null,
            isUpdate: -1,
            customerUpdate: {
                firstName: '',
                lastName: '',
                city: '',
                zip: ''
            }
        }
    }

    callAPI() {
        let url = `http://localhost:8080/customers`

        fetch(url)
            .then(result => result.json())
            .then(
                (data) => {
                    //console.log(data)
                    this.setState({ isLoaded: true, users: data })
                },
                (error) => {
                    this.setState({ isLoaded: true, error: error })
                }
            )
    }


    componentDidMount() {
        this.callAPI()
    }

    componentDidUpdate() {
        this.callAPI()
    }

    handleUpdateChange = (e, key) => {
        if (key === 'zip' && !Number(e.target.value) && e.target.value !== '') {
            return
        }

        const updatedCustomer = { ...this.state.customerUpdate }
        updatedCustomer[key] = e.target.value
        this.setState({ customerUpdate: updatedCustomer })
    }

    handleSubmit = (e, dateJoined, id) => {
        e.preventDefault()

        const updatedCustomer = { ...this.state.customerUpdate }
        updatedCustomer.dateJoined = dateJoined
        updatedCustomer.id = id

        fetch(`http://localhost:8080/customers`, {
            method: 'put',
            body: JSON.stringify(updatedCustomer),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(resp => {
            this.setState({ customerUpdate: {}, isUpdate: -1 })
        }).catch(error => {
            console.log(`Error: ${error}`)
        })
    }

    handleUpdateClick = (customer) => {
        const updatedCustomer = {
            firstName: customer.firstName,
            lastName: customer.lastName,
            city: customer.city,
            zip: customer.zip,
            dateJoined: customer.dateJoined,
            id: customer.id
        }

        this.setState({ isUpdate: customer.id, customerUpdate: updatedCustomer })
    }

    handleDelete = (e, customer) => {
        e.preventDefault()
        fetch(`http://localhost:8080/customers/${customer.id}`, {
            method: 'delete',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(resp => {
            this.setState({ customerUpdate: {} })
        }).catch(error => {
            console.log(`Error: ${error}`)
        })
    }

    render() {
        return (
            <div class="section">
                <h3 class="text-center">All Customers</h3>
                <br />
                <table class="table table-bordered table-hover">
                    <tr class="">
                        <th class="">First Name</th>
                        <th>Last Name</th>
                        <th>City</th>
                        <th>Zip</th>
                        <th>Date Joined</th>
                        <th>Number of Interactions</th>
                    </tr>
                    {this.state.users.map((user) => {
                        if (this.state.isUpdate === parseInt(user.id)) {
                            return (
                                <tr class="table-secondary">
                                    <td>
                                        <input
                                            value={this.state.customerUpdate.firstName}
                                            onChange={(e) => this.handleUpdateChange(e, 'firstName')}
                                            required
                                        />
                                    </td>

                                    <td>
                                        <input
                                            value={this.state.customerUpdate.lastName}
                                            onChange={(e) => this.handleUpdateChange(e, 'lastName')}
                                            required
                                        />
                                    </td>

                                    <td>
                                        <input
                                            value={this.state.customerUpdate.city}
                                            onChange={(e) => this.handleUpdateChange(e, 'city')}
                                            required
                                        />
                                    </td>

                                    <td>
                                        <input
                                            value={this.state.customerUpdate.zip}
                                            onChange={(e) => this.handleUpdateChange(e, 'zip')}
                                            maxLength="5"
                                            minLength="5"
                                            required
                                        />
                                    </td>

                                    <td>
                                        {user.dateJoined}
                                    </td>

                                    <td>
                                        {user.customerInteractions.length}
                                    </td>

                                    <td>
                                        <button onClick={(e) => this.handleSubmit(e, user.dateJoined, user.id)}>Submit</button>
                                        <button onClick={() => this.setState({ isUpdate: -1 })}>Cancel</button>
                                    </td>
                                </tr>
                            )
                        } else {
                            return (
                                <tr class="table-secondary">
                                    <td class>{user.firstName}</td>
                                    <td>{user.lastName}</td>
                                    <td>{user.city}</td>
                                    <td>{user.zip}</td>
                                    <td>{user.dateJoined}</td>
                                    <td>{user.customerInteractions.length}</td>
                                    <td><button class="btn btn-warning" onClick={() => this.handleUpdateClick(user)}>Update</button></td>
                                    <td><button class="btn btn-danger" onClick={(e) => this.handleDelete(e, user)}>Delete</button></td>
                                </tr>
                            )
                        }
                    })}
                </table>
            </div>
        )
    }
}