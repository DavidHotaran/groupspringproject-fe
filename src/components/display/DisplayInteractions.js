import React from 'react'
import './Display.css'


export class DisplayInteractions extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            users: [],
            isLoaded: false,
            error: null,
            interactionUpdate: {},
            isUpdate: -1
        } //intial state
    }

    callAPI() {
        let url = `http://localhost:8080/interactions`

        fetch(url)
            .then(result => result.json())
            .then(
                (data) => {
                    //console.log(data)
                    this.setState({ isLoaded: true, users: data })
                },
                (error) => {
                    this.setState({ isLoaded: true, error: error })
                }
            )
    }

    componentDidMount() {
        this.callAPI()
    }

    componentDidUpdate() {
        this.callAPI()
    }

    handleUpdateChange = (value, key) => {
        if (key === 'zip' && !Number(value) && value !== '') {
            return
        }

        const updatedInteraction = { ...this.state.interactionUpdate }
        updatedInteraction[key] = value
        this.setState({ interactionUpdate: updatedInteraction })
    }

    handleSubmit = (e, customerId) => {
        e.preventDefault()

        const updatedInteraction = { ...this.state.interactionUpdate }

        fetch(`http://localhost:8080/interactions/${customerId}`, {
            method: 'put',
            body: JSON.stringify(updatedInteraction),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(resp => {
            this.setState({ interactionUpdate: {}, isUpdate: -1 })
        }).catch(error => {
            console.log(`Error: ${error}`)
        })
    }

    handleUpdateClick = (interaction) => {
        this.setState({ isUpdate: interaction.id, interactionUpdate: interaction })
    }

    handleDelete = (e, id) => {
        e.preventDefault()
        fetch(`http://localhost:8080/interactions/${id}`, {
            method: 'delete',
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(resp => {
            this.setState({ isLoaded:true })
        }).catch(error => {
            console.log(`Error: ${error}`)
        })
    }

    render() {
        if (this.state.error) {
            return (
                <aside>
                    <h4>Error: {this.state.error.message}</h4>
                </aside>
            )
        } else if (!this.state.isLoaded) {
            return (
                <aside>
                    <h4>Waiting...</h4>
                    <button onClick={() => { this.callAPI() }}>Get the interactions</button>
                </aside>
            )
        } else {
            return (
                <div class="section">
                    <h3 class="text-center">All Interactions</h3>
                    <br />
                    <table class="table table-bordered table-hover">
                        <tr class="">
                            <th class>Customer Name</th>
                            <th class>Interaction Date:</th>
                            <th>Communication type</th>
                            <th>Comments</th>
                            <th>Resolved</th>
                        </tr>
                        {this.state.users.map(user => {
                            if (this.state.isUpdate === parseInt(user.id)) {
                                return (
                                    <tr class="table-secondary">
                                        <td class>{user.customer.firstName} {user.customer.lastName}</td>
                                        <td class>{user.interactionDate}</td>

                                        <td>

                                            <input
                                                value={this.state.interactionUpdate.communicationType}
                                                onChange={(e) => this.handleUpdateChange(e.target.value, 'communicationType')}
                                                required
                                            />
                                        </td>
                                        <td>
                                            <textarea
                                                value={this.state.interactionUpdate.comments}
                                                onChange={(e) => this.handleUpdateChange(e.target.value, 'comments')}
                                                required
                                            />
                                        </td>
                                        <td>
                                            <input
                                                type="checkbox"
                                                checked={this.state.interactionUpdate.resolution}
                                                onChange={(e) => this.handleUpdateChange(e.target.checked, 'resolution')}
                                            />
                                        </td>

                                        <td>
                                            <button onClick={(e) => this.handleSubmit(e, user.customer.id)}>Submit</button>
                                            <button onClick={() => this.setState({ isUpdate: -1 })}>Cancel</button>
                                        </td>
                                    </tr>
                                )
                            } else {
                                return (

                                    <tr class="table-secondary">
                                        <td class>{user.customer.firstName} {user.customer.lastName}</td>
                                        <td class>{user.interactionDate}</td>
                                        <td>{user.communicationType}</td>
                                        <td>{user.comments}</td>
                                        <td>{user.resolution.toString() === "false" ? "No" : "Yes"} </td>
                                        <td><button class="btn btn-warning" onClick={() => this.handleUpdateClick(user)}>Update</button></td>
                                        <td><button class="btn btn-danger" onClick={(e) => this.handleDelete(e, user.id)}>Delete</button></td>
                                    </tr>
                                )
                            }
                        })}
                    </table>
                </div>
            )
        }
    }
}