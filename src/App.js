import logo from './logo.svg'
import './App.css'
import AddCustomerForm from './components/forms/AddCustomerForm'
import AddInteractionForm from './components/forms/AddInteractionForm'
import { DisplayCustomers } from './components/display/DisplayCustomers'
import { DisplayInteractions } from './components/display/DisplayInteractions'

function App() {


  return (
    <section>
      <AddCustomerForm />
      <AddInteractionForm />
      <DisplayCustomers />
      <DisplayInteractions />
    </section>
  )
}

export default App
