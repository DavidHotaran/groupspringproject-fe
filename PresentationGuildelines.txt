General Guidelines
1. Each team has 20 mins
2. Everyone should speak - who did what
3. Demonstrate your project
4. Describe your architectural choices and why you selected them
5. Describe how you worked with your product owner
6. Describe how you worked as a team
7. What would you do if you had more time / what would you do differently if you were to start again?
8. You can use PPT if you want to or any other medium if you prefer
9. Expect questions as part of your 20 mins
