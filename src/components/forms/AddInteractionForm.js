import React, { useState, useEffect } from 'react'
import './Form.css'

function AddInteractionForm() {
    const [communicationType, setCommunicationType] = useState('')
    const [comments, setComments] = useState('')
    const [resolution, setResolution] = useState(false)
    const [customer, setCustomer] = useState(1)
    const [success, setSuccess] = useState(false)
    const [error, setError] = useState(false)
    const [customersList, setCustomersList] = useState([])

    useEffect(() => {
        fetch('http://localhost:8080/customers')
            .then(resp => resp.json())
            .then(
                (result) => {
                    setCustomersList(result)
                    let initCustomerId = result.length === 0 ? 1 : result[0].id
                    setCustomer(initCustomerId)
                },
                (err) => {
                    setError(true)
                }
            )
            .catch(err => setError(true))
    }, [customersList])

    const handleSubmit = (e) => {
        console.log(customer)
        e.preventDefault()
        const interaction = {
            communicationType: communicationType,
            interactionDate: new Date().toLocaleDateString(),
            comments: comments,
            resolution: resolution
        }

        fetch(`http://localhost:8080/interactions/${customer}`, {
            method: 'post',
            body: JSON.stringify(interaction),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(resp => {
            setCommunicationType('')
            setComments('')
            setResolution(false)
            setCustomer(1)
            setError(false)
            setSuccess(true)
        }).catch(error => {
            setSuccess(false)
            setError(true)
        })
    }

    return (
        <section className="formSection">
            <h3 className="formHeader">Add an Interaction</h3>

            <form id="interactionForm" className="form" onSubmit={(e) => handleSubmit(e)}>
                <section>
                    <section className="formInput">
                        <label>Customer: </label>
                        <select value={customer} onChange={(e) => setCustomer(e.target.value)}>
                            {customersList.map(customer => (
                                <option key={customer.id} value={customer.id}>{customer.firstName} {customer.lastName}</option>
                            ))}
                        </select>
                    </section>

                    <section className="formInput">
                        <label>Communication Type:</label>
                        <input
                            value={communicationType}
                            onChange={(e) => setCommunicationType(e.target.value)}
                            required
                        />
                    </section>

                    <section className="formInput">
                        <label>Comments:</label>
                        <textarea
                            value={comments}
                            onChange={(e) => setComments(e.target.value)}
                            required
                        />
                    </section>

                    <section className="formInput">
                        <label>Resolution:</label>
                        <input
                            type="checkbox"
                            checked={resolution}
                            onChange={(e) => setResolution(e.target.checked)}
                        />
                    </section>
                </section>

                <input type="submit" class="btn btn-success" value="Submit" />

                {
                    success ? (
                        <aside className="formSuccess">
                            Interaction successfully created!
                        </aside>
                    ) : error ? (
                        <aside className="formFailure">
                            Error: Something went wrong...!
                        </aside>
                    ) : (<section />)
                }
            </form>
        </section >
    )
}

export default AddInteractionForm