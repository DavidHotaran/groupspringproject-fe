import React, { useState } from 'react'
import './Form.css'

function AddCustomerForm() {
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [city, setCity] = useState('')
    const [zip, setZip] = useState('')
    const [error, setError] = useState(false)
    const [success, setSuccess] = useState(false)

    const handleZipChange = (zipChange) => {
        if (!Number(zipChange) && zipChange !== '') {
            return
        }

        setZip(zipChange)
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        const customer = {
            firstName: firstName,
            lastName: lastName,
            city: city,
            zip: parseInt(zip),
            dateJoined: new Date().toLocaleDateString()
        }

        fetch('http://localhost:8080/customers', {
            method: 'post',
            body: JSON.stringify(customer),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then((resp) => {
            setFirstName('')
            setLastName('')
            setCity('')
            setZip('')
            setError(false)
            setSuccess(true)
        }).catch((error) => {
            console.log(`Error: ${error}`)
            setSuccess(false)
            setError(true)
        })
    }

    return (
        <section className="formSection">
            <h3 className="formHeader">Add a Customer</h3>

            <form id="customerForm" className="form" onSubmit={(e) => handleSubmit(e)}>
                <section>
                    <section className="formInput">
                        <label>First Name:</label>
                        <input
                            value={firstName}
                            onChange={(e) => setFirstName(e.target.value)}
                        />
                    </section>

                    <section className="formInput">
                        <label>Last Name:</label>
                        <input
                            value={lastName}
                            onChange={(e) => setLastName(e.target.value)}
                        />
                    </section>

                    <section className="formInput">
                        <label>City:</label>
                        <input
                            value={city}
                            onChange={(e) => setCity(e.target.value)}
                        />
                    </section>

                    <section className="formInput">
                        <label>Zip Code:</label>
                        <input
                            value={zip}
                            onChange={(e) => handleZipChange(e.target.value)}
                            maxLength="5"
                            minLength="5"
                        />
                    </section>
                </section>

                <input type="submit" class="btn btn-success" value="Submit" />
            </form>
            {
                success ? (
                    <aside className="formSuccess">
                        Customer successfully created!
                    </aside>
                ) : error ? (
                    <aside className="formFailure">
                        Error: Something went wrong...!
                    </aside>
                ) : (<section />)
            }
        </section >
    )
}

export default AddCustomerForm